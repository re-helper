from django.conf.urls.defaults import *

# Uncomment the next two lines to enable the admin:
import main
from django.contrib import admin

urlpatterns = patterns('',
    (r'^.*', 'rehelper.main.page_handler'),
)
