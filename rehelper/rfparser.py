
'''This module is a red fin property page parser.

It retrieves property information from the page and returns a 'name': 'value' dictionary.

'''

import HTMLParser
import httplib
import re

space_filter = re.compile(r'\s+', re.M)

comment_block = re.compile(r'<!\-\-.*\-\->')

class RfParserError(Exception):
    pass

class RfReData(HTMLParser.HTMLParser):
    '''Red Fin real estate information parser.'''

    def __init__(self, debug=False):
        self.http = ''
        self.tag = ''
        self.prop_table = []
        self.reset()
        self.td_data = ''
        self.debug = debug

    def get_http(self, url):
        c = httplib.HTTPConnection("www.redfin.com:80")
        c.request("GET", url)
        r = c.getresponse()
        self.data = r.read()
        r.close()
        c.close()
        self.normalize_data()

    def get_file(self, file_name):
        self.data = open(file_name, 'r').read()
        self.normalize_data()

    def normalize_data(self):
        in_comment = False
        in_body = False
        t = []
        for l in self.data.splitlines():
            stripped = comment_block.sub('', l.strip()).strip()
            if not stripped:
                continue
            if in_comment:
                if '-->' in stripped:
                    in_comment = False
                    t.append(stripped.split('-->')[1])
                continue
            if '<!--' in stripped:
                in_comment = True
                t.append(stripped.split('<!--')[0])
                continue
            if in_body:
                t.append(stripped)
                continue
            if '<body' in stripped:
                in_body = True

        self.data = '\n'.join(t)

    def process_data(self):
        try:
            self.feed(self.data)
            self.close()
        except HTMLParser.HTMLParseError:
            pass
        d = {}
        for name, value in self.prop_table:
            d[name] = value
        return d

    def attr_to_dict(self, attrs):
        d = {}
        for n, v in attrs:
            d[n] = v
        return d

    def handle_data(self, data):
        if self.tag:
            d = space_filter.sub(' ', data).strip()
            if d:
                self.td_data += d

    def handle_tdata(self):
        if self.td_data:
            self.prop_table[-1].append(self.td_data.strip())
            self.td_data = ''

    def handle_starttag(self, tag, attrs):
        if self.debug:
            print '< ', tag
        if tag == 'div':
            attrd = self.attr_to_dict(attrs)
            if 'class' in attrd and attrd['class'] == 'price':
                self.td_data = ''
                self.tag = 'div'
                self.prop_table.append(['price'])

        if tag == 'table':
            attrd = self.attr_to_dict(attrs)
            if self.debug:
                print 'table', attrd
            if 'id' in attrd and attrd['id'] == 'property_basic_details':
                self.td_data = ''
                self.tag = tag
                return

        if tag == 'span':
            classes = ('street-address', 'postal-code')
            attrd = self.attr_to_dict(attrs)
            if 'class' in attrd and attrd['class'] in classes:
                self.td_data = ''
                self.tag = tag
                self.prop_table.append([attrd['class']])

        if not self.tag:
            return

        if tag == 'tr':
            self.handle_tdata()
            self.prop_table.append([])
            return

        if tag == 'td':
            self.handle_tdata()

    def handle_endtag(self, tag):
        if self.debug:
            print tag, '>'
        if tag == self.tag:
            self.tag = ''
            self.handle_tdata()
            if len(self.prop_table[-1]) == 0:
                self.prop_table = self.prop_table[:-1]
