
from django import forms
from google.appengine.ext import db
from google.appengine.ext.db.djangoforms import ModelForm

class Property(db.Model):
    addr = db.PostalAddressProperty(verbose_name='Property Address')
    geo = db.GeoPtProperty()
    beds = db.IntegerProperty(verbose_name='Number of Bedrooms')
    baths = db.IntegerProperty(verbose_name='Number of Bathrooms')
    sq_feet = db.IntegerProperty(verbose_name='SQ Footage')
    lot_sq_feet = db.IntegerProperty(verbose_name='Lot Size')
    price = db.IntegerProperty(verbose_name='Asking Price')
    website = db.LinkProperty(verbose_name='Property Website')
    prop_rating = db.RatingProperty(verbose_name='Property Rating')
    neighb_rating = db.RatingProperty(verbose_name='Neighborhood Rating')
    date_entered = db.DateProperty(auto_now_add=True)
    date_posted = db.DateProperty(auto_now_add=True)
    notes = db.StringProperty(
        verbose_name='Additional Information', multiline=True)
    owner = db.StringProperty()
    mls_number = db.IntegerProperty(verbose_name='MLS#')

class Owner(db.Model):
    name = db.StringProperty(verbose_name='Your Name', required=False)
    email = db.EmailProperty()
    rank = db.IntegerProperty()

class PropertyForm(ModelForm):
    widgets = (
        ('notes', forms.Textarea, {'cols': 45, 'rows': 10}),
        (('addr', 'website'), forms.TextInput, {'size': 60}),
        (('beds', 'baths', 'prop_rating', 'neighb_rating'),
         forms.TextInput, {'size': 1}),
        (('sq_feet', 'lot_sq_feet', 'price'), forms.TextInput, {'size': 10}),
        ('geo', forms.HiddenInput, {}),
        )

    class Meta:
        model = Property
        exclude = ('owner')

    def __init__(self, *args, **kwargs):
        super(PropertyForm, self).__init__(*args, **kwargs)
        for field_names, widget, dictionary in self.widgets:
            if type(field_names) == str:
                fields = (field_names,)
            else:
                fields = field_names
            for field_name in fields:
                self.fields[field_name].widget = widget(attrs=dictionary)

class OwnerForm(ModelForm):
    class Meta:
        model = Owner
        exclude = ('rank', 'email')

class SelfRegForm(OwnerForm):
    includeMe = forms.BooleanField(label='Please include me', required=False)

