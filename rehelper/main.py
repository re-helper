import logging
import re

from django import forms
from django import shortcuts
from django.http import HttpResponse
from django.http import HttpResponseRedirect
from django.template import Context
from django.template import loader

from google.appengine.api import users
from google.appengine.ext import db

import models
import rfparser

MAX_RANK = 100

form_from_redfin = {
    'addr': ('street-address', 'postal-code'),
    'beds': 'Beds:',
    'baths': 'Baths:',
    'sq_feet': 'Sq. Ft.:',
    'lot_sq_feet': 'Lot Size:',
    'price': 'price',
    'mls_number': 'MLS#:'
}

def get_redfin_data(rfs, url):
    '''rfs - list of fields to fill, url - RF address of the property page.'''
    result = {}
    p = rfparser.RfReData()
    p.get_http(url)
    rf_data = p.process_data()

    if not rf_data:
        return result

    number_only_re = re.compile(r'[0-9,]+')
    number_only_fields = ('sq_feet', 'lot_sq_feet', 'price')

    for field in rfs + ['lot_sq_feet', 'mls_number']:
        if field == 'geo':
            continue  # geo does not come from the website.
        rf_field = form_from_redfin[field]
        try:
            if type(rf_field) == str:
                result[field] = rf_data[rf_field]
                if field in number_only_fields:
                    so = number_only_re.search(rf_data[rf_field])
                    if not so:
                        del result[field]
                        continue
                    newv = so.group(0)
                    result[field] = newv.replace(',', '')
                continue
            # its a tuple
            result[field] = ' '.join(rf_data[x] for x in rf_field)
        except KeyError:
            logging.error('key error for %s' % rf_field)
    return result


class PageDescriptor(object):
    def __init__(self, tab_name, template_file, min_user_rank):
        self.tab_name = tab_name
        self.template_file = '%s.html' % template_file
        self.min_user_rank = min_user_rank

class PageDriver(object):
    nav_list_src = [
        ('home', PageDescriptor('Home', 'home', 0)),
        ('new_entry', PageDescriptor('Add property', 'add_property', 0)),
        ('map', PageDescriptor('Map', 'map', 0)),
        ('admin', PageDescriptor('Admin', 'base', 1)),
        ]

    def __init__(self, page_info, user_data):
        self.pure_path = page_info.strip('/')
        if not self.pure_path:
            self.pure_path = 'home'
        self.templ = ''
        self.context = Context({'nav_list': [],})
        self.user_record = None
        self.error_mode = False
        self.error_text = None
        if user_data.count() > 1:
            self.templ = 'error.html'
            self.context['error_message'] = 'Internal error!'
            self.error_mode = True
        elif user_data.count() == 1:
            self.user_record = user_data[0]
            self.context['full_name'] = self.user_record.name

    def prepare_home(self):
        if self.user_record is not None:
            owner = self.user_record.email
            props = db.GqlQuery(
                "SELECT * FROM Property WHERE owner = :1 order by addr", owner)
            if props.count():
                self.context['table_headers'] = ['Price', 'Address',
                                                 'SQ Feet', 'Bdrms', 'Bths']
                self.context['properties'] = props

    def prepare_admin(self):
        pass

    def prepare_map(self):
        if not self.user_record:
            return shortcuts.redirect('/')
        props = db.GqlQuery(
            "SELECT * FROM Property WHERE owner = :1", self.user_record.email)
        positions = []
        c_lat = 0.0
        c_lng = 0.0

        if props.count():
            for p in props:
                logging.info(p.addr)
                d = {}
                c_lat += p.geo.lat
                c_lng += p.geo.lon
                d['location'] = '%f, %f' % (p.geo.lat, p.geo.lon)
                if p.prop_rating:
                    d['icon'] = 'm%2.2d.png' % p.prop_rating
                else:
                    d['icon'] = 'm00.png'
                d['title'] = '%s; %d bd/%d bt, $%d' % (
                    p.addr.split(',')[0], p.beds, p.baths, p.price)
                if p.notes:
                    notes = '<div id="notes_contents">%s</div>' % (
                        '\\\n<br>'.join(p.notes.splitlines()))
                    notes = notes.replace("'", '&#39;')
                else:
                    notes = None
                d['notes'] = notes
                positions.append(d)
            self.context['map_center'] = '%s, %s' % (
                c_lat/props.count(), c_lng/props.count())
            self.context['positions'] = positions

    def prepare_new_entry(self):
        logging.info('before doing anything')
        pseudo_owner = '^%s^' % self.user_record.email
        saved = db.GqlQuery("select * FROM Property WHERE owner = :1",
                            pseudo_owner)
        logging.info('found %d entries for %s' % (saved.count(), pseudo_owner))
        self.context['do_addr_editing'] = True
        if saved.count():
            fixed_addr = False
            d = {}
            s = saved[0]
            if s.addr:
                # try getting the property to edit
                try:
                    p = models.Property.get(s.addr)
                    s = p
                    fixed_addr = True
                    self.context['do_addr_editing'] = False
                except db.BadKeyError:
                    pass # This was not a key, it was an address
            for field in s.fields():
                v = getattr(s, field)
                if v:
                    d[field] = v
            f = models.PropertyForm(d)
            drop_transient(pseudo_owner)
            if fixed_addr:
                d = f.fields['addr'].widget.attrs
                d['id'] = 'addr_field'
                d['readonly'] = 'readonly'
                logging.info('new attrs is %s' % d)
        else:
            f = models.PropertyForm()
        self.context['form'] = f

    def process_list(self):
        if self.error_mode:
            return

        if self.user_record is None:
            self.templ = 'new_user.html'
            # This user has not subscribed for the service yet, give him the
            # only page he needs.
            self.context['nav_list'] = [{'text': 'Home',
                                         'span_id': 'activelist'},]
            self.context['form'] = models.SelfRegForm()
            return

        if users.is_current_user_admin():
            user_rank = MAX_RANK
        else:
            user_rank = self.user_record.rank
        for path, pdesc in self.nav_list_src:
            if user_rank < pdesc.min_user_rank:
                continue
            d = {'text': pdesc.tab_name}
            if path == self.pure_path:
                self.templ = pdesc.template_file
                d['span_id'] = 'activelist'
            else:
                d['href'] = '/%s' % path
            self.context['nav_list'].append(d)

    def url_error(self):
        self.templ = 'error.html'
        self.context['error_message'
                     ] = '%s is not a valid page on this site' % self.pure_path
        del self.context['nav_list']

    def render(self):
        try:
            f = self.__getattribute__('prepare_%s' % self.pure_path)
        except AttributeError:
            f = self.url_error
        f()
        t = loader.get_template(self.templ)
        return HttpResponse(t.render(self.context))

def handle_new_user(req, user):
    nu_form = models.SelfRegForm(req.POST)
    nu_form.is_valid()
    include_me = nu_form.cleaned_data['includeMe']
    if include_me:
        new_user = models.Owner()
        new_user.email = user.email()
        new_user.name = nu_form.cleaned_data['name']
        new_user.rank = 0
        new_user.put()  # Add the new user to the database.
        return shortcuts.redirect('/')
    else:
        return shortcuts.redirect('http://www.endoftheinternet.com')

def handle_home(req, user):
    key = req.POST.get('edit', None)
    if key:
        prop = models.Property()
        prop.owner = '^%s^' % user.email()
        logging.info('setting addr to key %s' % key)
        prop.addr = str(key)
        prop.put()
        return shortcuts.redirect('/new_entry')
    for d in req.POST.getlist('delete'):
        try:
            p = models.Property.get(d)
            p.delete()
        except db.BadKeyError:
            pass # Just in case there is a doctored 'POST'
    return shortcuts.redirect('/')

def drop_transient(pseudo_owner):
    drops = db.GqlQuery("select * FROM Property WHERE owner = :1", pseudo_owner)
    logging.info('about to drop %d' % drops.count())
    for drop in drops:
        logging.info('drop is %s' % drop)
        drop.delete()

def handle_add_property(req, user):
    # Required fields.
    rfs = ['addr', 'beds', 'baths', 'sq_feet', 'price', 'geo']

    # Value entered by the post request
    query_prop = models.PropertyForm(req.POST)

    # Value to store valid fields to turn around post request
    prop = models.Property()

    for k, v in req.POST.iteritems():
        if not v:
            continue
        try:
            real_v = query_prop.fields[k].to_python(v)
        except forms.ValidationError:
            continue
        if k in rfs:
            rfs.remove(k)
        setattr(prop, k, real_v)

    if rfs or (prop.addr.count(',') != 2):
        # Either not al fields set or address ahs not beem normalized.
        if prop.website:
            pieces = prop.website.split('www.redfin.com')
            if len(pieces) == 2:
                redfin_data = get_redfin_data(rfs, pieces[1])
                for k, v in redfin_data.iteritems():
                    try:
                        real_v = query_prop.fields[k].to_python(v)
                    except forms.ValidationError:
                        logging.info('failed to validate %s for %s' % (v, k))
                        continue
                    setattr(prop, k, real_v)

        prop.owner = '^%s^' % user.email()
        prop.put()
        return shortcuts.redirect('/new_entry')

    # The form is prepared, needs to be either stored or edited
    recs = db.GqlQuery(
        "select * FROM Property WHERE addr = :1 AND owner = :2",
        prop.addr, user.email())
    for r in recs:
        r.delete()
    prop.owner = user.email()
    prop.put()
    return shortcuts.redirect('/')

post_dispatcher = {
    'new_user': handle_new_user,
    'add_property': handle_add_property,
    'home': handle_home
}

def handle_post(req, user):
    path = req.META['PATH_INFO'][1:]
    if path in post_dispatcher:
        return post_dispatcher[path](req, user)

    return shortcuts.redirect('/')


def page_handler(req):
    user = users.get_current_user()
    if not user:
        return shortcuts.redirect(users.create_login_url("/"))
    logging.info('user email is %s' % user.email())

    user_data = db.GqlQuery(
        "SELECT * FROM Owner WHERE email = :1", user.email())

    if req.method == 'POST':
        return handle_post(req, user)

    pdr = PageDriver(req.META['PATH_INFO'], user_data)
    pdr.process_list()
    return HttpResponse(pdr.render())
