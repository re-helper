import os
import sys

# Remove the provided version of Django (if any)
for k in [k for k in sys.modules if k.startswith('django')]:
    del sys.modules[k]

# Fix path to include 1_2 instead of 0_96
NP = []
for p in sys.path:
    if 'django_0_96' in p: # running locally
        p = p.replace('_0_96', '_1_2')
    if 'django-0.96' in p: # running on the server
        p = p.replace('-0.96', '-1.2')
    NP.append(p)
sys.path = NP

# Must set this env var before importing any part of Django
os.environ['DJANGO_SETTINGS_MODULE'] = 'rehelper.settings'

# Google App Engine imports.
from google.appengine.ext.webapp import util

# Force Django to reload its settings.
from django.conf import settings
settings._target = None

import django.core.handlers.wsgi

def main():
    # Create a Django application for WSGI.
    application = django.core.handlers.wsgi.WSGIHandler()

    # Run the WSGI CGI handler with that application.
    util.run_wsgi_app(application)

if __name__ == '__main__':
    main()
